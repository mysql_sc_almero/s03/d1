-- CRUD OPERATINS --

--CREATE
INSERT INTO artists (name) VALUES("Rivermaya");
INSERT INTO artists (name) VALUES("Psy");

INSERT INTO albums (album_title, date_released, artist_id) VALUES(
	"Psy 6",
	"2012-1-1",
	2
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES(
	"Trip",
	"1996-1-1",
	1
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Gangnam Style",
	253,
	"K-pop",
	1
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Kundiman",
	234,
	"OPM",
	2
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Kisapmata",
	259,
	"OPM",
	2
);


--READ
--Display all songs and all attributes
SELECT * FROM songs;

--Display all songs and song name, genre
SELECT song_name, genre FROM songs;

--Display all songs from OPM genre
SELECT song_name FROM songs WHERE genre = "OPM";

--Display all songs that have duration longer than 2min40sec
--We can use AND or OR keywords
SELECT song_name FROM songs WHERE length > 240 AND genre = "OPM";



--UPDATE
-- Update length of Kundiman to 240
UPDATE songs SET length = 240 WHERE song_name = "Kundiman";

--Update all song length to 240
UPDATE songs SET length = 240;





--DELETE
--Delete all OPM Songs longer than 2:40
DELETE FROM songs WHERE genre = "OPM" AND length > 240;